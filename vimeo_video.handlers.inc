<?php

/**
 * Gets the thumbnail url for vimeo video.
 *
 * @param int $video_id
 *   The vimeo video id.
 *
 * @return array
 *   The video thumbnail information.
 */
function vimeo_video_handle_get_thumbnail($video_id) {
  $request = _vimeo_video_get_setting('domain') . "api/v2/video/$video_id.json";
  $video = _vimeo_video_response_decode_json($request, TRUE);

  return $video;
}

/**
 * @return array
 * 	Return default settings
 */
function _vimeo_video_default_settings() {
  return array(
	'domain' => 'http://vimeo.com/',
  );
}

/**
 * @param $setting
 * 	A name from default settings list
 *
 * @return str|bool
 * 	Return default setting by name if it is exist and FALSE if not exist
 */
function _vimeo_video_get_setting($setting) {
  $settings = _vimeo_video_default_settings();

  return $settings[$setting] ?: FALSE;
}

/**
 * Gets a video id by username and title of the video.
 *
 * @param $username
 * 	Usename of user who had added the video to vimeo.com
 *
 * @param $video_title
 * 	Title of video
 *
 * @return array|bool
 * 	An array of video id, or FALSE on error
 */
function _vimeo_video_get_id_needed_video_by_username_and_title($username, $video_title) {
  $request = _vimeo_video_get_setting('domain') . "api/v2/$username/videos.json";
  $videos = _vimeo_video_response_decode_json($request);

  if (count($videos) > 1) {
	foreach($videos as $video) {
	  if (strtolower($video->title) === $video_title) {
		return array(
		  'id' => $video->id,
		);
	  }
	}
  }

  return FALSE;
}

/**
 * Get response from API vimeo.com
 *
 * @param $request
 * 	Request string for API
 *
 * @param bool $current
 * 	Check if get only current element
 *
 * @return array|bool
 * 	An array of response, or FALSE on error
 */
function _vimeo_video_response_decode_json($request, $current = FALSE) {
  $response = drupal_http_request($request);
  if (!isset($response->error)) {
	$response = json_decode($response->data);

	return $current ? (array) current($response) : (array) $response;
  }

  return FALSE;
}
