<?php if (isset($prefix)): ?>
  <?php print $prefix; ?>
<?php endif; ?>

<a href="<?php print $video_url; ?>" title="<?php print $title; ?>" target="_blank">
  <img src="<?php print $image_url; ?>" alt="<?php print $title; ?>" title="<?php print $title; ?>" />
</a>

<?php if (isset($suffix)): ?>
  <?php print $suffix; ?>
<?php endif; ?>
