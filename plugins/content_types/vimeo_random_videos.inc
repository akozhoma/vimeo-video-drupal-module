<?php

$plugin = array(
  'single' => TRUE,
  'title' => t('Vimeo random videos'),
  'description' => t('Shows random videos from vimeo.com which were saved on the site.'),
  'category' => t('Social media'),
  'edit form' => 'vimeo_video_random_videos_edit_form',
  'render callback' => 'vimeo_video_random_videos_render',
  'admin info' => 'vimeo_video_random_videos_admin_info',
  'defaults' => array(
	'video_to_show' => 3,
  )
);

/**
 * 'admin info' callback for panel pane.
 */
function vimeo_video_random_videos_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
	$block = new stdClass;
	$block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
	$block->content = t('Showing @video_to_show videos from <em>vimeo.com</em>.', array(
	  '@video_to_show' => $conf['video_to_show'],
	));

	return $block;
  }
}

/**
 * 'Edit form' callback for the content type.
 */
function vimeo_video_random_videos_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['video_to_show'] = array(
	'#title' => t('Number of videos to show'),
	'#description' => t('Used to control the number of videos shown on the page initially. Defaults to 3.'),
	'#type' => 'select',
	'#options' => drupal_map_assoc(range(2, 10)),
	'#default_value' => $conf['video_to_show'],
	'#required' => TRUE,
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function vimeo_video_random_videos_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
	if (isset($form_state['values'][$key])) {
	  $form_state['conf'][$key] = $form_state['values'][$key];
	}
  }
}

/**
 * Run-time rendering of the body of the block (content type)
 */
function vimeo_video_random_videos_render($subtype, $conf, $panel_args, $context = NULL) {
  $block = new stdClass();
  $content = '';

  // initial content is blank
  $block->title = '';
  $block->content = '';

  // get stored video ids
  $video_ids = vimeo_video_get_random_stored_videos_ids($conf['video_to_show']);

  foreach($video_ids as $video_id) {
	$thumbnail = vimeo_video_handle_get_thumbnail($video_id);
	$content .= theme('vimeo_video_thumbnail_show', array(
	  'image_url' => $thumbnail['thumbnail_large'],
	  'video_url' => $thumbnail['url'],
	  'title' => $thumbnail['title'],
	  'prefix' => '<div class="vimeo-video-container">',
	  'suffix' => '</div>',
	));
  }

  if ($content !== '') {
	drupal_add_css(drupal_get_path('module', 'vimeo_video') . '/css/vimeo.css');
	$block->content = $content;
  }

  return $block;
}
